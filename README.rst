=======
cacheit
=======


Save websites offline, including the resources.


Description
===========

``cacheit`` allows you to save websites offline, including things such as the resources and assets. You can also specify a depth, to allow some limited navigation between pages.

For example::

    cacheit --url https://ollybritton.com

With 'cache' the homepage of ``https://ollybritton.com`` offline and save it into a new directory. This includes things such as images and scripts that the page requires to run.

What's more::

    cacheit --depth 2 --url https://ollybritton.com

Will 'cache' the homepage offline, but will also cache all links on that page, so that you can click on each link to go to the next page.


Note
====

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
