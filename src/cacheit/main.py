#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import argcomplete
import sys
import logging

from cacheit import __version__

__author__ = "Olly Britton"
__copyright__ = "Olly Britton"
__license__ = "mit"

_logger = logging.getLogger(__name__)


def parse_args(args):
    """Parse command line parameters"""

    parser = argparse.ArgumentParser(
        description="Cache webpages offline."
    )

    parser.add_argument(
        '--version',
        action='version',
        version='cacheit {ver}'.format(ver=__version__)
    )

    parser.add_argument(
        '--url',
        '-u',
        help="Specify the URL to download offline.",

        dest="url",
        type=str,
    )

    parser.add_argument(
        '--depth',
        '-d',
        help="Decides how far the program should go; For example, --depth 2 will cache links on the URL specified. --depth 3 will cache all links on the links on the URL specified.",

        dest="depth",
        type=int,

        default=1
    )

    parser.add_argument(
        '-v',
        '--verbose',
        help="Enable a command-line output.",

        dest="verbose",
        action='store_const',
        const=True
    )

    argcomplete.autocomplete(parser)
    return parser.parse_args(args)


def main(args):
    args = parse_args(args)

    print(args.url, args.depth)


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
